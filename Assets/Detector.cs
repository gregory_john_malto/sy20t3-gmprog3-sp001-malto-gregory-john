﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        
        this.GetComponentInParent<Enemy>().setTarget(other.gameObject);
       
    }

    private void OnTriggerExit(Collider other)
    {
        
        this.GetComponentInParent<Enemy>().removeTarget(other.gameObject);
        // access the remove target 

    }
}
