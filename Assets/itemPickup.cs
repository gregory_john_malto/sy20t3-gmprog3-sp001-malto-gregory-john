﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPickup : MonoBehaviour
{
	public Item item;
	private void OnTriggerEnter(Collider other)
	{
	
		//PICK UP SYSTEM
		Debug.Log("PLAYER PICKED UP");
		bool wasPickedUp = Inventory.instance.Add(item);

		if(wasPickedUp)
		Destroy(gameObject);
	}
}
