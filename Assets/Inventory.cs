﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	public static Inventory instance;
	public GameObject inventoryObject;

	void Awake()
	{
		if(instance != null)
		{
			Debug.LogWarning("More than one inventory instance!");
			return;
		}
		instance = this;
	}

	void Update()
	{
		if (Input.GetKeyDown("i"))
		{
			if (inventoryObject.activeSelf == false)
			{
				inventoryObject.SetActive(true);
			}

			else
			{
				inventoryObject.SetActive(false);
			}
		}
	}
	
	public int space = 4;
	public delegate void OnItemChanged();
	public OnItemChanged onItemChangedCallback;

	public List<Item> items = new List<Item>();

	public bool Add(Item item)
	{
			if(items.Count >= space)
			{
				Debug.Log("Not enough space.");
				return false;
			}
			items.Add(item);

			if (onItemChangedCallback != null)
			onItemChangedCallback.Invoke();
		
		return true;
	}



	public void Remove(Item item)
	{
		items.Remove(item);

		if (onItemChangedCallback != null)
			onItemChangedCallback.Invoke();
	}
}
