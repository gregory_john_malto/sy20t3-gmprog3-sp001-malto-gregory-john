﻿
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
	public Image icon;
	Item item;
		public Player playerStats;
	public void addItem(Item newItem)
	{
		item = newItem;

		icon.sprite = item.icon;
		icon.enabled = true;
	}

	public void ClearSlot()
	{
		item = null;

		icon.sprite = null;
		icon.enabled = false;  
	}



	public void useItem()
	{
		if(item!=null)
		{
			if (playerStats.curHP < playerStats.maxHP)
			{
				playerStats.curHP += 50;
				Inventory.instance.Remove(item);
			}
			else
			{
				Debug.Log("HP is full.");
			}


		}
	}
}
