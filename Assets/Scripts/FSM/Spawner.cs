﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> enemies = new List<GameObject>();
    public List<Enemy> enemyList = new List<Enemy>();
    public float spawnTime = 1f;
    public int spawnedEnemies = 0;
    int maxSpawn = 10;





    void Start()
    {
  
        InvokeRepeating("Spawn", spawnTime, spawnTime);

    }

    void Update()
    {
    }

    void SpawnEnemy()
    {
       
        
    }
 
    public void Spawn()
    {
        int x = Random.Range(1, 50);
        int z = Random.Range(1, 50);
        int y = 5;
        Vector3 pos = new Vector3(x, y, z);

        if (spawnedEnemies < maxSpawn)
        {
            Instantiate(enemies[Random.Range(0, 3)], pos, Quaternion.identity);
            spawnedEnemies++;

         

        }
    }

    public void removeEnemy()
    {
        spawnedEnemies--;
    }
}

