﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;


public class Enemy : MonoBehaviour
{
	[SerializeField] EnemyStates curState;



	public NavMeshAgent Agent;
	public Animator enemyAnimator;
	public Transform target;
	public GameObject Player;
	//public GameObject LootDrop;/// <summary>
	/// /////////
	/// </summary>
	public Vector3 targetPlayer;
	public Vector3 patrolPoint;


	public float idleTimer = 4;
	public float movementSpeed;
	public bool destinationReached = false;

	
	public float maxHP;
	public float curHP;
	public float STR;
	public int atkPwr;
	public float VIT;
	public int LootDrop;
	public int expReward;
	//hp bar
	[SerializeField] Image hpBar;
	public enum EnemyStates
	{
		Idle,
		Patrol,
		Chase,
		Attack,
		Dying,
		Death,
	}

	void Start()
	{
		//target = GetComponent<Transform>();
		maxHP = VIT * 5;
		curHP = maxHP;
		Agent = gameObject.GetComponent<NavMeshAgent>();
		enemyAnimator = this.GetComponent<Animator>();
		Agent.speed = movementSpeed;
		curState = EnemyStates.Idle;
		enemyAnimator.SetBool("Idle", true);

	}

	void Update()
	{
		hpBar.fillAmount = curHP / maxHP;
		switch (curState) //updates states per frame
		{
			case EnemyStates.Idle: IdleUpdate(); break;
			case EnemyStates.Patrol: PatrolUpdate(); break;
			case EnemyStates.Attack: AttackUpdate(); break;
			case EnemyStates.Chase: ChaseUpdate(); break;
			case EnemyStates.Dying: DyingUpdate(); break;
			case EnemyStates.Death: DeathUpdate(); break;
		}

	}

	public void IdleUpdate()
	{
		enemyAnimator.SetInteger("AnimIndex", 0);

		idleTimer -= Time.deltaTime;

		if (idleTimer <= 0)
		{
			idleTimer = 4;
			curState = EnemyStates.Patrol;
		}

		if (target != null)
		{
			curState = EnemyStates.Chase;
			initPatrol = false;
		}
	}

	bool initPatrol = false;

	public void PatrolUpdate()
	{
		enemyAnimator.SetInteger("AnimIndex", 6);
		//call the random once
		if (!initPatrol)
		{
			randomPos();
			initPatrol = true;
		}


		if (Agent.hasPath && Agent.remainingDistance <= Agent.stoppingDistance + 0.08f)
		{
			curState = EnemyStates.Idle;
			initPatrol = false;
		}

		if (target != null)
		{
			curState = EnemyStates.Chase;
			initPatrol = false;
		}
	}

	public void ChaseUpdate()
	{
		transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform, Vector3.up);
		enemyAnimator.SetInteger("AnimIndex", 5); //
		chasePlayer();
	}

	public void AttackUpdate()
	{
		// if target is in range change animation to attack
		if (Vector3.Distance(this.transform.position, target.transform.position) > 2)
		{
			curState = EnemyStates.Chase;
		}
		else
		{
			curState = EnemyStates.Idle;
		}

		transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
		enemyAnimator.SetInteger("AnimIndex", 4); //play attack
												  //attack(playerTarget_);
	}


	void DyingUpdate()
	{
		enemyAnimator.SetInteger("AnimIndex", 3);
		Destroy(gameObject, 3);
		GameObject.Find("Spawner").GetComponent<Spawner>().removeEnemy();
		curState = EnemyStates.Death;
		
		//Player.GetComponent<Player>().gainExp(expReward);
		//return;
	}

	void DeathUpdate()
	{
		
	}


	public void randomPos()
	{
		patrolPoint = new Vector3(Random.Range(Random.Range(-7, -2), Random.Range(2, 7)), 0, Random.Range(Random.Range(-7, -2), Random.Range(2, 7)));
		patrolPoint += transform.position;

		Agent.SetDestination(patrolPoint);
	}

	public void chasePlayer()
	{
		targetPlayer = target.transform.position;

		Agent.SetDestination(targetPlayer);

		if (Vector3.Distance(this.transform.position, target.transform.position) < 2)
		{
			curState = EnemyStates.Attack;
		}
		//switch to attack
	}

	//create a public function that sets the target
	public void setTarget(GameObject curTarget)
	{
		if (curTarget.gameObject.name == "Koko")
		{
			target = curTarget.gameObject.transform;
			return;
		}
	}

	public void removeTarget(GameObject curTarget)
	{
		if (curTarget.gameObject.name == "Koko")
		{
			target = null;
			curState = EnemyStates.Patrol;
			return;
		}
	}
	
	public void TakeDamage(int damage, GameObject damager)
	{
		if(target == null)
		{
			curState = EnemyStates.Dying;
			return;
		}
		damager = target.gameObject;
		
		curHP -= damage;
		if (curHP <= 0)
		{
		
			//damager.GetComponent<Player>();
			if(damager.GetComponent<Player>())
			{
				damager.GetComponent<Player>().gainExp(expReward);
				Destroy(gameObject);

				
				//ItemDrop = GetComponent<LootDrop>();
				GetComponent<LootDrop>().calculateLoot();
				// Instantiate(LootDrop , transform.position, Quaternion.identity);
				Debug.Log("Enemy died");
			}
			curState = EnemyStates.Dying;
		}
	}

	void DamageTo()
	{
		
		target.GetComponent<Player>().takeDamage(atkPwr, gameObject);
	}
}
