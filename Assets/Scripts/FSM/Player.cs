﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
	public int maxHP;
	public int curHP;

	public int maxMP;
	public int curMP;

	
	public int STR;
	public int atkPwr;
	public int VIT;

	public int curEXP;
	public int nextEXP;
	public int excessEXP;
	public Quest quest;
	public Transform target;

	public Animator playerAnimator;

	int gainedExp;
	public int playerLvl;
	int carryOverXP;

	public GameObject levelUpButtons;
	// Use this for initialization

	[SerializeField] Image hpBar;
	[SerializeField] Image expBar;
	NavMeshAgent Agent;
	
	void Start()
	{
		Agent = GetComponent<NavMeshAgent>();

		STR = atkPwr;
		maxHP = VIT * 5;
		curHP = maxHP;
		curMP = maxMP;
	}

	// Update is called once per frame
	void Update()
	{
		STR = atkPwr;
		maxHP = VIT * 5;

		hpBar.fillAmount = (float)curHP / (float)maxHP;
		expBar.fillAmount = (float)curEXP / (float)nextEXP;

		if (curHP>=maxHP)
		{
			curHP = maxHP;
		}

		if (curMP >= maxMP)
		{
			curMP = maxMP;
		}

		if (target != null)
		{
			attack();
		}

		if (curEXP >= nextEXP)
		{
			nextEXP += 50;
			//curEXP = 0;

		
			//curEXP = gainedExp;
			playerLvl++;


			Debug.Log("Leveled up" + curEXP + "/" + nextEXP);
			levelUpButtons.SetActive(true);
		}

		//LevelUp();

		if (curHP <= 0)
		{
			playerAnimator.SetInteger("AnimIndex", 6); //dead
			Destroy(gameObject, 2);

		}
	}

	void DamageTo()
	{
		if(target.GetComponent<Enemy>().curHP <= 0)
		{
			target = null;
		}

		if (target == null)
			return;
		target.GetComponent<Enemy>().TakeDamage(atkPwr, gameObject);
	}

	public void attack() //attack target
	{
		float dist = Vector3.Distance(this.transform.position, target.transform.position);
		//if (dist <= Agent.remainingDistance)//( < 1.2 || target != null)
		//{
		//playerAnimator.SetInteger("AnimIndex", 1); //run
		//transform.LookAt(GameObject.FindGameObjectWithTag("Enemy").transform);

		playerAnimator.SetInteger("AnimIndex", 2); //play attack           
		//}
	}

	public void setTarget(GameObject curTarget)
	{
		if (curTarget.gameObject.tag == "Enemy")
		{
			target = curTarget.gameObject.transform;
			return;
		}
	}

	public void removeTarget(GameObject curTarget)
	{
		if (curTarget.gameObject.tag == "Enemy")
		{
			target = null;
			return;
		}
	}

	public void gainExp(int expReward)
	{
		curEXP += expReward;
	    
		Debug.Log("Reward Exp is " + expReward);

		Debug.Log("Gained Exp.");
		Debug.Log("Current Exp is." + curEXP);
	}

	public void takeDamage(int damage, GameObject damager)
	{
		//damager = target.gameObject;
		curHP -= damage;
		
		
	}

	public void cheatLevelUp()
	{
		curEXP += 100;
		Debug.Log("Exp Status: "+curEXP + "/" + nextEXP);
	}

	public void VITUpgrade()
	{
		VIT += 1;
		levelUpButtons.SetActive(false);
	}

	public void STRUpgrade()
	{
		STR += 1;
		levelUpButtons.SetActive(false);
	}

	public void LevelUp()
	{

	}
}
