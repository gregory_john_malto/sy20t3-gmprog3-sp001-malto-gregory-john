﻿using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName =  "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
	public string itemName;
	public Sprite icon;
}
