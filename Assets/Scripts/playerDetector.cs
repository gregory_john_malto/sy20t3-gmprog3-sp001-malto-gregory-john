﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerDetector : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        
        this.GetComponentInParent<Player>().setTarget(other.gameObject);
       
    }

    private void OnTriggerExit(Collider other)
    {
      
        this.GetComponentInParent<Player>().removeTarget(other.gameObject);
     

    }
}
