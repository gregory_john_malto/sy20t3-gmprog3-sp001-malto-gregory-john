﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.AI;

public class clickMove : MonoBehaviour
{
    NavMeshAgent agent;
    public GameObject pointer;
    public Animator animator;
    public GameObject enemyIndicator;
   

    void Start()
    {
        animator.SetInteger("AnimIndex", 0);
        agent = GetComponent<NavMeshAgent>();
        pointer.SetActive(false);
    }

    void Update()
    {
		if (EventSystem.current.IsPointerOverGameObject())
			return;

        if (Input.GetMouseButtonDown(0))
        {
			//Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                if (hit.transform.tag == "Enemy")
                {
                    animator.SetInteger("AnimIndex", 1); //walk

                    agent.destination = hit.point;


                    pointer.SetActive(false);

                    
                    this.GetComponent<Player>().setTarget(hit.collider.gameObject);
                    keepTarget();

                    if (agent.remainingDistance <= agent.stoppingDistance + 2f)
                    {
                        this.GetComponent<Player>().attack();
                    }                    
                }
                else if (hit.transform.tag != "Enemy")
                {
                    animator.SetInteger("AnimIndex", 1); //walk
                    agent.destination = hit.point;
                    pointer.transform.position = hit.point;
                    enemyIndicator.SetActive(false);
                    pointer.SetActive(true);

                    this.GetComponent<Player>().target = null;
                }

			}
        }

        if(pointer.activeSelf == false || agent.remainingDistance == 0.5f)
        {
            animator.SetInteger("AnimIndex", 0); //play idle
        }
        
    }

    public void keepTarget()
    {
        enemyIndicator.SetActive(true);
        enemyIndicator.transform.position = GetComponent<Player>().target.transform.position;
    }
}


