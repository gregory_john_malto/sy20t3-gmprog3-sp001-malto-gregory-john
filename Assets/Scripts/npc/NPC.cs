﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
	public Button button;
	public GameObject Player;
	public GameObject NPCcanvas;
	private bool triggeringNPC;
	private bool shopEnabled;


	public GameObject item01;
	public GameObject item02;
	public GameObject item03;
	public int price;

	public void Start()
	{
		Button btn = button.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}

	public void Update()
	{
		if (shopEnabled)
			NPCcanvas.SetActive(true);
		else
			NPCcanvas.SetActive(false);
		if(triggeringNPC)///press E
		{
			if (Input.GetKeyDown(KeyCode.E))
				shopEnabled = !shopEnabled;

		}
	}
	void TaskOnClick()
	{
		///print("shop");
		if(shopEnabled)
		{
			//print("shop");//
			Sell01();

			Sell02();
			
			Sell03();
		}
	}

	public void Sell01()
	{
		if (Player.GetComponent<PlayerMoney>().Money >= price)
		{
			Transform itemSpawned =Instantiate(item01.transform, Player.transform.position, Quaternion.identity);
		

			itemSpawned.gameObject.SetActive(false);
			itemSpawned.parent = Player.transform;
			Player.GetComponent<PlayerMoney>().Money -= price;
		}
		else
		{
			print("not enough money");
		}
	}

	public void Sell02()
	{
		if (Player.GetComponent<PlayerMoney>().Money >= price)
		{
			Transform itemSpawned = Instantiate(item02.transform, Player.transform.position, Quaternion.identity);


			itemSpawned.gameObject.SetActive(false);
			itemSpawned.parent = Player.transform;
			Player.GetComponent<PlayerMoney>().Money -= price;
		}
		else
		{
			print("not enough money");
		}
	}

	public void Sell03()
	{
		if (Player.GetComponent<PlayerMoney>().Money >= price)
		{
			Transform itemSpawned = Instantiate(item03.transform, Player.transform.position, Quaternion.identity);


			itemSpawned.gameObject.SetActive(false);
			itemSpawned.parent = Player.transform;
			Player.GetComponent<PlayerMoney>().Money -= price;
		}
		else
		{
			print("not enough money");
		}
	}
	public void OnTriggerEnter(Collider other)
	{
		
		if (other.tag == "Player")
		{
			triggeringNPC = true;
			print("hi");
		}
	}


	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			triggeringNPC = false;
		}
	}
}
