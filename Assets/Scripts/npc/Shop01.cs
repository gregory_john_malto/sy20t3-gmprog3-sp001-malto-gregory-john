﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop01 : MonoBehaviour
{
	public GameObject ShopInventory;
	public GameObject Item01text;
	public static bool lockCursor;
	public GameObject ItemCompletion;
	public GameObject CompleteText;

	void OntriggerEnter()
	{
		ShopInventory.SetActive(true);
		
		Cursor.visible = false;
		GlobalShop.ShopNumber = 1;
		Item01text.GetComponent<Text>().text = "" + GlobalShop.Item01;
	}

	public void Item01()
	{
		ItemCompletion.SetActive(true);
		CompleteText.GetComponent<Text>().text = "Sir pasa mo na ako" + GlobalShop.Item01 + "!";
	}

	public void CancelTransaction()
	{
		ItemCompletion.SetActive(false);
	}

	
}
