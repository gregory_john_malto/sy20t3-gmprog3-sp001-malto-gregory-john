﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlUp : MonoBehaviour
{
	public int playerLvl;
	public int curEXP;
	public int nextExp;

	private void Start()
	{
		playerLvl = 1;
		curEXP = 0;
		nextExp = 150;
	}

	private void Update()
	{
		if (curEXP >= nextExp)
		{
			curEXP -= nextExp;
			playerLvl++;
			//nextExp += 30;
			Debug.Log("Lvl: " + playerLvl + " - " + curEXP + "/" + nextExp);
		}
	}

	public void giveExp(int expReward)
	{
		curEXP += expReward;
		Debug.Log("Lvl: " + playerLvl + " - " + curEXP + "/" + nextExp);
	}
}
