﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcAnimator : MonoBehaviour
{

    public Animator animator;

    public GameObject NPC;

    public void setFalse()
    {

    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Idle()
    {
        animator.SetInteger("AnimIndex", 0);
    }
    public void Talk()
    {
        animator.SetInteger("AnimIndex", 1);
    }
}
