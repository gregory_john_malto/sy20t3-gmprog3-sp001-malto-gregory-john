﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootDrop : MonoBehaviour
{
	[System.Serializable]
	public class loot
	{
		public string name;
		public GameObject item;
		public int dropRarity;
	}

	public List<loot> lootTable = new List<loot>();
	public int dropChance;

	// call the calculateloot in the enemy script to drop an item

	public void calculateLoot()
	{
		int calc_dropChance = Random.Range(0, 101);

		if (calc_dropChance > dropChance)
		{
			Debug.Log("no drops");
			return;
		}

		if(calc_dropChance <= dropChance )
		{
			int itemWeight = 0;

			for(int i = 0; i <lootTable.Count; i++)
			{
				itemWeight += lootTable[i].dropRarity;

			}
			Debug.Log("ItemWeight=" + itemWeight);

			int randomValue = Random.Range(0, itemWeight);

		    for (int j = 0; j< lootTable.Count; j++)
			{
				if(randomValue <= lootTable[j].dropRarity)
				{
					Instantiate(lootTable[j].item, transform.position, Quaternion.identity);
					return;
				}

			}
		}
	}

	//public int[] table =
	//{
	//	33,//hp potion
	//	33,// mana po
	//	33// gold?
	//};
	//public int total;
	//public int randomNumber;


  // private void Start()
  //  {
		//foreach(var item in table)
		//{
		//	total += item;
		//}

		//randomNumber = Random.Range(0, total);
  //  }

  
    
}
