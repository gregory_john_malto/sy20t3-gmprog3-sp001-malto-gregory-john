﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monsterController : MonoBehaviour
{
    public Animator animator1;
    public Animator animator2;
    public Animator animator3;

    public GameObject Monster1;
    public GameObject Monster2;
    public GameObject Monster3;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Idle()
    {
        animator1.SetInteger("AnimIndex", 0);
        animator2.SetInteger("AnimIndex", 0);
        animator3.SetInteger("AnimIndex", 0);
    }
    public void Attack()
    {
        animator1.SetInteger("AnimIndex", 4);
        animator2.SetInteger("AnimIndex", 4);
        animator3.SetInteger("AnimIndex", 4);
    }
    public void Damage()
    {
        animator1.SetInteger("AnimIndex", 2);
        animator2.SetInteger("AnimIndex", 2);
        animator3.SetInteger("AnimIndex", 2);
    }
    public void Dead()
    {
        animator1.SetInteger("AnimIndex", 3);
        animator2.SetInteger("AnimIndex", 3);
        animator3.SetInteger("AnimIndex", 3);
    }

    public void Run()
    {
        animator1.SetInteger("AnimIndex", 5);
        animator2.SetInteger("AnimIndex", 5);
        animator3.SetInteger("AnimIndex", 5);
    }

    public void Skill()
    {
        animator1.SetInteger("AnimIndex", 1);
        animator2.SetInteger("AnimIndex", 1);
        animator3.SetInteger("AnimIndex", 1);
    }

    public void Walk()
    {
        animator1.SetInteger("AnimIndex", 6);
        animator2.SetInteger("AnimIndex", 6);
        animator3.SetInteger("AnimIndex", 6);
    }
}
