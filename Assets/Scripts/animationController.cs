﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationController : MonoBehaviour
{
    public Animator animator;
    public GameObject toBeAnimated;
    // Use this for initialization
	// Update is called once per frame

	void Update ()
    {
       
	}
    public void Idle ()
    {
        animator.SetInteger("AnimIndex", 0);
    }
	public void Run()
	{
		animator.SetInteger("AnimIndex", 1);
	}
	public void Attack()
    {
        animator.SetInteger("AnimIndex", 2);
    }
    public void Standby()
    {
        animator.SetInteger("AnimIndex", 3);
    }
    public void Combo()
    {
        animator.SetInteger("AnimIndex", 4);
    }
    public void Damage()
    {
        animator.SetInteger("AnimIndex", 5);
    }
    public void Dead()
    {
        animator.SetInteger("AnimIndex", 6);
    }

    public void drawBlade()
    {
        animator.SetInteger("AnimIndex", 7);
    }

    public void putBlade()
    {
        animator.SetInteger("AnimIndex", 8);
    }

    public void runNo()
    {
        animator.SetInteger("AnimIndex", 10);
    }

    public void Skill()
    {
        animator.SetInteger("AnimIndex", 11); 
    }
}
