﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStats : MonoBehaviour
{
	public Player playerStats;
	//public levelUPScript lvlUpStats;
	public int displayStatsInt;

	public Text displayStats;
	void Start()
	{
		displayStats = GetComponent<Text>();

		//maxHP = GetComponent<float>.maxHP;
	}

	// Update is called once per frame
	void Update()
	{
		switch (displayStatsInt)
		{
			case 1:
				displayHP();
				break;

			case 2:
				displayVit();
				break;

			case 3:
				displayAtk();
				break;

			case 4:
				displayStr();
				break;

			case 5:
				displayLvl();
				break;

			case 6:
				displayExp();
				break;

			case 7:
				displayMaxHp();
				break;

			case 8:
				displayExpReq();
				break;

			case 9:
				displayMp();
				break;

			case 10:
				displayMaxMp();
				break;
		}
	}

		void displayHP()
		{
			displayStats.text = playerStats.curHP.ToString();
		}

		void displayVit()
		{
			displayStats.text = playerStats.VIT.ToString();
		}

		void displayAtk()
		{
			displayStats.text = playerStats.atkPwr.ToString();
		}

		void displayStr()
		{
			displayStats.text = playerStats.STR.ToString();
		}

		void displayLvl()
		{
			displayStats.text = playerStats.playerLvl.ToString();
		}

		void displayExp()
		{
			displayStats.text = playerStats.curEXP.ToString();
		}

		void displayMaxHp()
		{
			displayStats.text = playerStats.maxHP.ToString();
		}

		void displayExpReq()
		{
			displayStats.text = playerStats.nextEXP.ToString();
		}

		void displayMp()
		{
			displayStats.text = playerStats.curMP.ToString();
		}

		void displayMaxMp()
		{
			displayStats.text = playerStats.maxMP.ToString();
		}
	}

