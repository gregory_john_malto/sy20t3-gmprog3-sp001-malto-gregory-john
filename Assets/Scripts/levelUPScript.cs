﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelUPScript : MonoBehaviour {

	public Player playerStats;
	public GameObject lvlUpUI;
	public int remainingExp;
	public int xpToLvl;
	public int xpSetNewThresh;
	int carryOverXP;

	// Use this for initialization
	void Start()
	{

	}


	void Update()
	{
		xpToLvl = remainingExp - playerStats.curEXP;
		for (int i = playerStats.playerLvl; playerStats.curEXP >= remainingExp; playerStats.playerLvl++)
		{
			carryOverXP = playerStats.curEXP - remainingExp;
			remainingExp *= xpSetNewThresh;
			playerStats.curEXP = 0 + carryOverXP;
			lvlUpBonus();
			lvlUpUI.SetActive(true);
			
		}

		//xpToLvl = remainingExp - playerStats.curEXP;
	}

	public void receiveXp(int exp)
	{
		playerStats.curEXP += exp;
	}

	void lvlUpBonus()
	{
		playerStats.VIT += 1;
		playerStats.STR += 1;
		playerStats.curHP = playerStats.maxHP;
		playerStats.curMP = playerStats.maxMP;
	}
}
