﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonCamera : MonoBehaviour
{
    Vector3 playerPos = new Vector3(8, 4, 13);
    Vector3 monsterPos = new Vector3(-4, 4, 13);
    Vector3 npcPos = new Vector3(-19, 4, 13);

    public GameObject playerButtons;

    public GameObject monsterButtons;

    public GameObject npcButtons;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void playerCam()
    {
        npcButtons.SetActive(false);
        monsterButtons.SetActive(false);
        transform.position = playerPos;
        playerButtons.SetActive(true);
    }

    public void monsterCam()
    {
        npcButtons.SetActive(false);
        playerButtons.SetActive(false);
        transform.position = monsterPos;
        monsterButtons.SetActive(true);
    }

    public void npcCam()
    {
        monsterButtons.SetActive(false);
        playerButtons.SetActive(false);
        transform.position = npcPos;
        npcButtons.SetActive(true);
    }
}
